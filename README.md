# TRIOS Wubicons 

Wubicon emoticons pack.  
Original wubicons smiley theme, extended by FOSS Serbia community.  
Tested and working with Pidgin, Gajim, Kopete, Telepathy.  

## Installation:

Either use provided "install" script, or manually copy "TRIOS Wubicons" dir to:  
- Pidgin: `~/.purple/smileys` or system wide in `/usr/share/pixmaps/pidgin/emotes`  
- Gajim: `/usr/share/gajim/data/emoticons`  
- Kopete/Telepathy: `/usr/share/emoticons`

## Configuration:

**Pidgin:**

Go to Tools --> Preferences --> Themes and select "TRIOS Wubicons" as the smiley theme.  

**Gajim:**

Go to Edit --> Preferences and select "TRIOS Wubicons" as the emoticons theme.  

**Kopete/KDE-Telepathy:**

Go to KDE System Settings --> Application appearance --> Emoticons, select "TRIOS Wubicons" from the list and hit Apply.
That will set the theme for both Kopete and KDE-Telepathy.